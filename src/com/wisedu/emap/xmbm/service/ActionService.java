package com.wisedu.emap.xmbm.service;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wisedu.emap.mvc.AbstractPageIntervention;

@Service("91f53d2b-598a-41b1-b53f-82446d2e531a")
public class ActionService extends AbstractPageIntervention {
	
	@Autowired
	private ApiService apiService;

	/** method example
	 ************************************************************
	 * 干预动作的执行.
	 * @param param   当前执行的数据操作动作所需的参数
	 * @param action  当前需要执行的动作(动作流节点中选中的动作)
	 * @return  执行动作后的返回结果
	 *
	 ************************************************************
	public Object interveneExecute(DaoParam param, ActionWrapper action) {
		return action.getAction().execute(param);
	}
	*/
	
	public Map<String, Object> getStudentDetail(String stuNumber) {
		Map<String, Object> map = new HashMap<String, Object>();
		String detail = apiService.getUsersWithDeptDetail2(stuNumber);
		JSONObject json = JSONObject.fromObject(detail);
		map.put("ID", json.get("id"));
		map.put("NAME", json.get("name"));
		map.put("GENDER", json.get("sexName"));
		map.put("SCHOOL", json.get("academyName"));
		map.put("MAJOR", json.get("majorName"));
		map.put("GRADE", json.get("grade"));
		map.put("ID_NUMBER", json.get("identityCode"));
		return map;
	}
	
}
