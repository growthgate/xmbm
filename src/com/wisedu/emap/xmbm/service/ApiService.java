package com.wisedu.emap.xmbm.service;

import java.io.IOException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import self.micromagic.util.annotation.Config;

@Component
public class ApiService {

	/* 基本配置 */
	@Config(name = "api.url", description = "总线服务器域名")
	private static String apipath;
	@Config(name = "accessToken", description = "接口accessToken")
	private static String accessToken;
	@Config(name = "appId", description = "应用id")
	private static String appId;
	@Config(name = "charset", description = "编码")
	private static String charset;
	
	/**
	 * 本科生基本信息
	 * @param stuNumber
	 * @return
	 */
	public String getUndergraduateInfo(String stuNumber) {
		// 基础参数
		String name = "cs_auth_web/rest/common/student/getUndergraduateInfo";
		String serviceId = "f1fd5dde237e";
		String url = getRealUrl(name);
		
		// 拼装查询参数
		JSONObject param = new JSONObject();
		param.put("XH", stuNumber);
		
		JSONObject params = new JSONObject();
		params.put("param", param);
		
		return findFirstREST(url, serviceId, params, "result");
//		return baseREST(url, serviceId, params);
	}
	
	/**
	 * 用户详细信息
	 * @param stuNumber
	 * @return
	 */
	public String getUsersWithDeptDetail2(String stuNumber) {
		// 基础参数
		String name = "cs_auth_web/restful/users/getUsersWithDeptDetail2";
		String serviceId = "08c594023765";
		String url = getRealUrl(name);
		
		// 拼装查询参数
		JSONObject page = new JSONObject();
		page.put("pageSize", 10);
		page.put("pageNum", 1);
		
		JSONObject params = new JSONObject();
		params.put("ids", new String[]{stuNumber});
		params.put("page", page);
		
		return findFirstREST(url, serviceId, params, "userInfoWithDepts");
//		return baseREST(url, serviceId, params);
	}
	
	public String getUsersWithDeptDetail2() {
		// 基础参数
		String name = "cs_auth_web/restful/users/getUsersWithDeptDetail2";
		String serviceId = "08c594023765";
		String url = getRealUrl(name);
		
		// 拼装查询参数
		JSONObject page = new JSONObject();
		page.put("pageSize", 10);
		page.put("pageNum", 1);
		
		JSONObject params = new JSONObject();
		params.put("ids", new String[]{"1402020091"});
		
		return baseREST(url, serviceId, params);
	}
	
	/**
	 * 调用RESTful API并获取第一条数据
	 * @param url
	 * @param serviceId
	 * @param params
	 * @param resultName
	 * @return
	 */
	private String findFirstREST(String url, String serviceId, JSONObject params, String resultName) {
		String result = "{}";
		String retStr = baseREST(url, serviceId, params);
		JSONObject json = JSONObject.fromObject(retStr);
		try {
			if (json.get(resultName) instanceof JSONArray) {
				JSONArray array = json.getJSONArray(resultName);
				if (!array.isEmpty()) {
					result = array.getString(0);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 基本RESTful调用方法
	 * @param url 请求地址
	 * @param serviceId 服务id
	 * @param params 请求参数
	 * @return 返回的json数据
	 */
	private String baseREST(String url, String serviceId, JSONObject params) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		if (params == null) {
			params = new JSONObject();
		}
		StringEntity entity = new StringEntity(params.toString(), charset);
		entity.setContentEncoding(charset);
		entity.setContentType("application/json");
		
		// 获取调用方法
		HttpPost method = new HttpPost(url);
		method.setEntity(entity);
		method.setHeader("appId", appId);
		method.setHeader("accessToken", accessToken);
		if (serviceId != null) {
			method.setHeader("serviceId", serviceId);
		}
		
		// 方法调用
		CloseableHttpResponse result = null;
		String retStr = null;
		try {
			result = httpClient.execute(method);
			HttpEntity ret = result.getEntity();
			retStr = EntityUtils.toString(ret);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (result != null) {
					result.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return retStr;
	}
	
	/**
	 * 根据服务名称获取接口实际访问地址
	 * @param url
	 * @return
	 */
	protected String getRealUrl(String name) {
		name = name.replaceAll("/", "-");
		StringBuilder url = new StringBuilder();
		url.append(apipath).append("/mdm_").append(name).append("/ProxyService/").append(name).append("ProxyService");
		return url.toString();
	}
	
}
