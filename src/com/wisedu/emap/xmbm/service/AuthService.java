package com.wisedu.emap.xmbm.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import com.wisedu.emap.auth.IAuthManager;
import com.wisedu.emap.pedestal.app.AppBeanContainer;

@Component
public class AuthService {

	private AppBeanContainer<IAuthManager> authMgr = new AppBeanContainer<IAuthManager>("emapAuth", IAuthManager.BEANID, false);
	
	public void addRole() {
		IAuthManager tmp = authMgr.get();
		// 角色列表
		Map<String, String> map = new HashMap<String, String>();
		map.put("gjc", "国际处");
		map.put("xy", "学院");
		map.put("xs", "学生");
		for (Entry<String, String> entry : map.entrySet()) {
			tmp.modifyRole(entry.getKey(), entry.getValue());
			tmp.getRole(entry.getKey()).addSubRole(entry.getKey() + "gn, " + entry.getKey() + "sj");
		}
	}

}
