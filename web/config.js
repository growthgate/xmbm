﻿define(function(require, exports, module) {

	var config = {

		/*
		 * 业务线开发模式，转测时置false
		 */
		//"DEBUG_MODE": true,
		/*
		 * 1.2版本
		 */
		"BH_VERSION": "1.2",

		/*
		 * 资源服务器地址
		 */
		"RESOURCE_SERVER": "http://res.wisedu.com",

		/*
		 * 主题 blue purple
		 */
		"THEME": "purple",

		/*
			服务器端生成配置API(API_BASE_PATH目录下)
			@example "/config.do" ./mock/serverconfig.json
		 */
		"SERVER_CONFIG_API": "/emap/sys/emapcomponent/getConfig.do",

		/*
			APP默认路由
		 */
		'APP_ENTRY': "",

		/*
		 	APP标题
		 */
		"APP_TITLE": "项目报名",

		/*
			应用底部说明文本
		 */
		"FOOTER_TEXT": "",

		/*
			需要展示的模块
		 */
		/*"MODULES": [
		 {            title:"字典管理",            route:"zdgl"         }         ,{            title:"暑期项目报名",            route:"sqxmbm"         }         ,{            title:"暑期项目管理",            route:"sqxmgl"         }         ,{            title:"系统常量配置",            route:"xtclpz"         }         ,{            title:"交换生合作学校管理",            route:"jhshzxxgl"         }         ,{            title:"交换生报名",            route:"jhsbm"         }         ,{            title:"项目报名限制",            route:"xmbmxz"         }         ,{            title:"暑期项目审核",            route:"sqxmsh"         }         ,{            title:"暑期项目报名限制",            route:"sqxmbmxz"         }         ,{            title:"交换生报名限制",            route:"jhsbmxz"         }         ,{            title:"暑期项目审核",            route:"sqxmsh"         }         ,{            title:"交换生审核",            route:"jhssh"         }         ,{            title:"学生年级管理",            route:"xsnjgl"         }         ,{            title:"学年学期管理",            route:"xnxqgl"         }         ,{            title:"交换生审核",            route:"xyjhssh"         }         ,{            title:"暑期项目审核",            route:"xysqxmsh"         }         //placeHolder_module
		],*/

		/*
			外层框架初始化结束，进入模块前执行的回调
		 */
		"AFTER_FRAMEWORK_INIT": function(){
			$.fn.serializeJson = function() {
				var serializeObj = {};
				var array = this.serializeArray();
				var str = this.serialize();
				$(array).each(function() {
					if (serializeObj[this.name]) {
						if ($.isArray(serializeObj[this.name])) {
							serializeObj[this.name].push(this.value);
						} else {
							serializeObj[this.name]=[serializeObj[this.name],this.value];
						}
		            } else {
						serializeObj[this.name]=this.value;
					}    
				});
				return serializeObj;
			};
	    },
		
		/*
			头部配置
		 */
		"HEADER": {
			"dropMenu": [{
				"text": "角色1",
				"active": true
			}],
			"logo": "./public/images/logo.png",
			"icons": ["icon-apps"],
			"userImage": "./public/images/user.png",
			"userInfo": {
				"image": "./public/images/user.png",
				"info": [
					"工号",
					"姓名 性别",
					"学校 部门",
					"邮箱",
					"电话"
				],
				"logoutHref": "javascript:void(0);"
			}
		}
	};

	return config;

});