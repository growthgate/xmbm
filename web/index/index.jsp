<%@ page language="java" contentType="text/html; charset=UTF-8" session="false" %>
<%@ taglib uri="/WEB-INF/etags/emap.tld" prefix="e" %>
<!DOCTYPE html>
<html lang="zh-CN">
<%
	String RES_HOST = com.wisedu.emap.base.core.EmapContext.getStaticResourceRoot();
%>
<head>

<meta charset="UTF-8">
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

<!-- commomlib include jquery.js jquery.nicescroll.js jquery.fileupload.js director.min.js hogan.min.js lodash.min.js globalize.js-->
<script src="<%= RES_HOST %>/fe_components/commonlib.js"></script>
<!-- 此处可以放置第三方库begin -->
<!-- jQuery 防止Emap自带jQuery未加载而后续代码报错的情况 -->
<!-- <script>!window.jQuery && document.write('<script src="<%= RES_HOST %>/fe_components/jquery-1.11.3.js"><\/script>')</script> --!>
<!-- jQuery-Validation-Engine -->
<link rel="stylesheet" type="text/css" href="<%= RES_HOST %>/bower_components/summernote-0.8.1/dist/summernote-bs3.min.css">
<link rel="stylesheet" type="text/css" href="<%= RES_HOST %>/bower_components/summernote-0.8.1/dist/summernote.css">
<link rel="stylesheet" type="text/css" href="<%= RES_HOST %>/bower_components/jQuery-Validation-Engine/css/validationEngine.jquery.css">
<script src="<%= RES_HOST %>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<%= RES_HOST %>/bower_components/summernote-0.8.1/dist/summernote.js"></script>
<script src="<%= RES_HOST %>/bower_components/jQuery-Validation-Engine/js/jquery.validationEngine.js"></script>
<script src="<%= RES_HOST %>/bower_components/jQuery-Validation-Engine/js/languages/jquery.validationEngine-zh_CN.js"></script>
<!-- 此处可以放置第三方库end -->
<script src="<%= RES_HOST %>/fe_components/appcore-min.js"></script>

<!-- 全局变量pageMeta等 -->
<script type="text/javascript">
var pageMeta = <e:page/>;
var contextPath = "<%= request.getContextPath() %>";
</script>

</head>

<body>


</body>

</html>
