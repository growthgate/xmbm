define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./zdglBS');

    var viewConfig = {
        initialize: function() {
        	var mode = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_DICT_QUERY', 'form');
            $("#emapForm").emapForm({
                root:WIS_EMAP_SERV.getContextPath(),//附件上传时必备属性
                data: mode,
                model: 'h'
            });
            
            this.eventMap = {
                '[data-action=save]': this.save
            };
        },
        save: function(){
        	if( $("#emapForm").emapValidate('validate') ){
        		var formData = $("#emapForm").emapForm("getValue");
        		//$("#emapForm").emapForm("saveUpload");//上传附件时使用
        		bs.save(formData).done(function(data){
        			$.bhTip({
        			    content: '数据保存成功！',
        			    state: 'success'
        			 });
                    $.bhPaperPileDialog.hide({
                    	close: function() {
                    		$('#emapdatatable').emapdatatable('reload');
                    	}
                    });//关闭当前弹窗
    			});
        	}
        }

    };
    return viewConfig;
});