﻿define(function(require, exports, module) {
	var utils = require('utils');
	var bs = {
		api: {
			pageModel: 'modules/zdgl.do',
			resultProfile: './mock/resultProfile.json'
		},
		getDemoMainModel: function() {
			var def = $.Deferred();
			utils.doAjax(bs.api.resultProfile, null, 'get').done(function(data) {
				data.length = data.list.length;
				def.resolve(data);
			}).fail(function(res) {
				def.reject(res);
			});
			return def.promise();
		},
		save: function(formData){
			return BH_UTILS.doAjax('../modules/zdgl/T_XMBM_DICT_SAVE.do', formData);
		},
		del: function(params){
			return BH_UTILS.doAjax('../modules/zdgl/T_XMBM_DICT_DELETE.do', {
				T_XMBM_DICT_DELETE:JSON.stringify(params)
			});
		},
		exportData: function(obj){
			var params = {
					root: contextPath,
					app : "xmbm",
					module : "modules",
					page : 'zdgl',
					action : 'T_XMBM_DICT_QUERY'
			};
			//选择字段导出
			$('#emapdatatable').emapdatatable('selectColumnsExport', params);	
		}
	};

	return bs;
});