﻿define(function(require, exports, module) {

  var utils = require('utils');
  var bs = require('./zdglBS');
  var zdglSave = require('./zdglSave');
  var zdglView = require('./zdglView');

  var viewConfig = {
    initialize: function() {
      var view = utils.loadCompiledPage('zdgl');
      this.$rootElement.html(view.render({}), true);
      this.pushSubView([zdglSave]);
      this.initView();

      this.eventMap = {
        "[data-action=add]": this.actionAdd,
        "[data-action=edit]": this.actionEdit,
        "[data-action=detail]": this.actionDetail,
        "[data-action=delete]": this.actionDelete
      };
    },

    initView: function() {
      this._initAdvanceQuery();
      this._initTable();
    },

    actionAdd: function(){
      var zdglNewTpl = utils.loadCompiledPage('zdglSave');
      $.bhPaperPileDialog.show({
        content: zdglNewTpl.render({}),
        title: "新建",
        ready: function($header, $body, $footer){
          zdglSave.initialize();
        }
      });
    },
        
    actionEdit: function(e){
      var id = $(e.target).attr("data-x-wid");
      var zdglEditTpl = utils.loadCompiledPage('zdglSave');
      var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_DICT_QUERY', {WID:id, pageNumber:1});
          
      $.bhPaperPileDialog.show({
        content: zdglEditTpl.render({}),
        title: "编辑",
        ready: function($header, $body, $footer){
          zdglSave.initialize();       
          $("#emapForm").emapForm("setValue", data.rows[0]);    
        }
      });
    },
        
    actionDetail: function(e){
      var id = $(e.target).attr("data-x-wid");
      var zdglViewTpl = utils.loadCompiledPage('zdglSave');
      var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_DICT_QUERY', {WID:id, pageNumber:1});
          
      $.bhPaperPileDialog.show({
        content: zdglViewTpl.render({}),
        title: "查看",
        ready: function($header, $body, $footer){
          zdglView.initialize(data.rows[0]);
        }
      });
    },
        
    actionDelete: function(){
      var row = $("#emapdatatable").emapdatatable("checkedRecords");
      if(row.length > 0){
        var params = row.map(function(el){
          return {WID:el.WID};  //模型主键
        });
        bs.del(params).done(function(data){
        	$.bhTip({
			    content: '数据删除成功！',
			    state: 'success'
			});
        	$('#emapdatatable').emapdatatable('reload');
        });
      }
    },
        
    _initAdvanceQuery: function() {
      var searchData = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_DICT_QUERY', "search");
      var $query = $('#emapAdvancedQuery').emapAdvancedQuery({
        data: searchData,
        contextPath : contextPath,
        schema: true
      });
      $query.on('search', this._searchCallback);
    },

    _searchCallback: function(e, data, opts) {
      $('#emapdatatable').emapdatatable('reloadFirstPage', {
        querySetting: data
      });
    },

    _initTable: function() {
      var tableOptions = {
        pagePath: bs.api.pageModel,
        action: 'T_XMBM_DICT_QUERY',
        customColumns: [{
          colIndex: '0',
          type: 'checkbox'
        }, {
          colIndex: '1',
          type: 'tpl',
          column: {
            text: '操作',
            align: 'center',
            cellsAlign: 'center',
            cellsRenderer: function(row, column, value, rowData) {
              return '<a href="javascript:void(0)" data-action="detail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>'
                + ' | <a href="javascript:void(0)" data-action="edit" data-x-wid=' + rowData.WID + '>' + '编辑' + '</a>';
            }
          }
        }]
      };
      $('#emapdatatable').emapdatatable(tableOptions);
    }
  };

  return viewConfig;
});