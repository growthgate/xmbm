define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./sqxmglBS');
    var proId = null;

    var viewConfig = {
        initialize: function(data) {
        	proId = data.WID;
        	/*
        	 * 项目信息
        	 */
        	var mode = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_MGR_QUERY', 'form');
            $("#emapForm").emapForm({
                root:WIS_EMAP_SERV.getContextPath(),
                data: mode,
                model: 'h',
                readonly:true
            });
            $("#emapForm").emapForm("setValue", data);
            $('[data-name="DESCRIPTION"]').replaceWith('<div class="note-editor"><div class="note-editable panel-body">' + data.DESCRIPTION + '</div></div>');
            /*
             * 报名限制
             */
            $('#limitDatatable').emapEditableDataTable({
            	emapdatatable: {
            		pagePath: bs.api.pageModel,
            		action: 'hqsqxmxzlb',
            		params: {
            			PRO_ID: proId
            		},
            		pageable: false,
            		minLineNum: 0,
            		customColumns: [{
    					colIndex: '0',
    					type: 'checkbox'
    				}]
            	}
            });
            
            this.eventMap = {
            	"[data-action=limitEdit]": this.limitEdit,
            	"[data-action=limitAdd]": this.limitAdd,
            	"[data-action=limitDelete]": this.limitDelete,
            	"[data-action=limitSave]": this.limitSave,
            	"[data-action=limitCancel]": this.limitCancel
            };
        },
        limitEdit: function() {
        	$('#limitDatatable').emapEditableDataTable('enterEditMode');
        	$('#limitButton').hide();
        	$('#limitEditButton').show();
        },
        limitAdd: function() {
        	$('#limitDatatable').emapEditableDataTable('addRow', {
        		newRows: [{
        			PRO_ID: proId
        		}]
        	});
        },
        limitDelete: function() {
        	var row = $("#limitDatatable").emapEditableDataTable('getEmapDataTable').emapdatatable("checkedRecords");
        	if(row.length > 0){
                var params = row.map(function(el){
                	return {WID:el.WID};  //模型主键
                });
                bs.delLimit(params).done(function(data){
                	$.bhTip({
        			    content: '数据删除成功！',
        			    state: 'success'
        			});
                	$('#limitDatatable').emapEditableDataTable('getEmapDataTable').emapdatatable('reload');
                });
              }
        },
        limitSave: function() {
        	var $table = $('#limitDatatable');
        	if ($table.emapEditableDataTable('validateTable')) {
        		// 新增的数据
				var newChangeRows = $table.emapEditableDataTable('getNewChangedRows');
				// 修改的数据
				var changeRows = $table.emapEditableDataTable('getChangedRows');
				bs.saveLimit(newChangeRows.concat(changeRows)).done(function(data) {
					viewConfig.limitCancel();
					$table.emapEditableDataTable('getEmapDataTable').emapdatatable('reload');
				});
			} else {
				$.bhTip({
					state: 'warning',
					content: '存在非法输入'
				});
			}
        },
        limitCancel: function() {
        	$('#limitDatatable').emapEditableDataTable('leaveEditMode');
			$('#limitButton').show();
			$('#limitEditButton').hide();
        }
    };
    return viewConfig;
});