define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./sqxmglBS');
    var applyView = require('./applyView');
    var applyAudit = require('./applyAudit');
    var applySave = require('./applySave');

    var viewConfig = {
        initialize: function(id) {
        	var safe = this;
        	/*
        	 * 待审核
        	 */
        	$('#tableStatusWait').emapdatatable({
				pagePath: bs.api.pageModel,
				action: 'cxsqxmbmlb',
				params: {
					PROJECT: id,
					STATUS: '1'
				},
				customColumns: [{
					colIndex: '0',
					type: 'tpl',
					column: {
						text: '操作',
						align: 'center',
						cellsAlign: 'center',
						cellsRenderer: function(row, column, value, rowData) {
							return '<a href="javascript:void(0)" data-action="applyDetail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>'
									+ ' | <a href="javascript:void(0)" data-action="audit" data-x-wid=' + rowData.WID + '>' + '审核' + '</a>';
						}
					}
				}]
        	});
        	/*
        	 * 审核通过
        	 */
        	$('#tableStatusPass').emapdatatable({
				pagePath: bs.api.pageModel,
				action: 'cxsqxmbmlb',
				params: {
					PROJECT: id,
					STATUS: '3'
				},
				customColumns: [{
					colIndex: '0',
					type: 'checkbox'
				}, {
					colIndex: '1',
					type: 'tpl',
					width: '180px',
					column: {
						text: '操作',
						align: 'center',
						cellsAlign: 'center',
						cellsRenderer: function(row, column, value, rowData) {
							return '<a href="javascript:void(0)" data-action="applyDetail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>'
									+ ' | <a href="javascript:void(0)" data-action="finish" data-x-wid=' + rowData.WID + '>' + '项目完成' + '</a>'
									+ ' | <a href="javascript:void(0)" data-action="stop" data-x-wid=' + rowData.WID + ' data-x-sid=' + rowData.STU_NUMBER + '>' + '项目中止' + '</a>';
						}
					}
				}]
        	});
        	/*
        	 * 项目完成
        	 */
        	$('#tableStatusFinish').emapdatatable({
				pagePath: bs.api.pageModel,
				action: 'cxsqxmbmlb',
				params: {
					PROJECT: id,
					STATUS: '10'
				},
				customColumns: [{
					colIndex: '0',
					type: 'tpl',
					column: {
						text: '操作',
						align: 'center',
						cellsAlign: 'center',
						cellsRenderer: function(row, column, value, rowData) {
							return '<a href="javascript:void(0)" data-action="applyDetail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>';
						}
					}
				}]
        	});
        	/*
        	 * 项目未完成
        	 */
        	$('#tableStatusFail').emapdatatable({
				pagePath: bs.api.pageModel,
				action: 'cxsqxmbmlb',
				params: {
					PROJECT: id,
					STATUS: '-1,-3,-10'
				},
				customColumns: [{
					colIndex: '0',
					type: 'tpl',
					column: {
						text: '操作',
						align: 'center',
						cellsAlign: 'center',
						cellsRenderer: function(row, column, value, rowData) {
							return '<a href="javascript:void(0)" data-action="applyDetail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>';
						}
					}
				}]
        	});
        	
        	$('#applyTab').jqxTabs();
        	$('#applyTab').on('tabclick', function() {
        		safe.reloadDatatable();
        	});
        	
        	this.eventMap = {
        		"[data-action=applyDetail]": this.applyDetail,
        		"[data-action=audit]": this.audit,
        		"[data-action=finish]": this.finish,
        		"[data-action=stop]": this.stop,
        		"[data-action=dialogSubmit]": this.dialogSubmit,
        		"[data-action=dialogClose]": this.dialogClose,
        		"[data-action=applyAdd]": this.applyAdd,
        		"[data-action=proFinish]": this.proFinish
            };
        },
        applyDetail: function(e) {
        	var id = $(e.target).attr("data-x-wid");
            var applyViewTpl = utils.loadCompiledPage('applyView');
            var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_QUERY', {WID:id});
                
            $.bhPaperPileDialog.show({
              content: applyViewTpl.render({}),
              title: "查看",
              ready: function($header, $body, $footer){
            	  applyView.initialize(data.rows[0]);
              }
            });
        },
        audit: function(e) {
        	var id = $(e.target).attr("data-x-wid");
            var applyAuditTpl = utils.loadCompiledPage('applyView');
            var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_QUERY', {WID:id, pageNumber:1});
                
            $.bhPaperPileDialog.show({
              content: applyAuditTpl.render({}),
              title: "审核",
              ready: function($header, $body, $footer){
            	  applyAudit.initialize(data.rows[0]);
              }
            });
        },
        finish: function(e) {
        	var id = $(e.target).attr("data-x-wid");
    		bs.savePro({WID: id, STATUS: 10}).done(function(data){
            	$.bhTip({
    			    content: '项目完成！',
    			    state: 'success'
    			});
            	$('#tableStatusPass').emapdatatable('reload');
            });
        },
        stop: function(e) {
        	var id = $(e.target).attr("data-x-wid");
        	var stuNum = $(e.target).attr("data-x-sid");
            // 打开预置弹窗
            $.bhPropertyDialog.show();
            $.bhPropertyDialog.footerShow();
            
            var mode = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_STOPPED_PRO_QUERY', 'form');
            $("#dialogInfoForm").emapForm({
            	root:WIS_EMAP_SERV.getContextPath(),//附件上传时必备属性
            	data: mode,
            	model: 'v'
            });
            $("#dialogInfoForm").emapForm('setValue', {
            	PRO_TYPE: 'summer',
            	PRO_ID: id,
            	STU_NUMBER: stuNum
            });
            
            // 教师姓名默认为当前老师
        	utils.doAjax("../modules/sqxmgl/hqyhxxxx.do", {}).done(function(data) {
        		var info = data.datas.hqyhxxxx;
        		$("#dialogInfoForm").emapForm('setValue', {
        			TEACHER: info.NAME
            	});
        	});
        },
        dialogSubmit: function() {
        	var safe = this;
        	if ($("#dialogInfoForm").emapValidate('validate')) {
        		var formData = $("#dialogInfoForm").emapForm("getValue");
        		bs.stop({formData: JSON.stringify(formData)}).done(function(data){
        			$.bhTip({
        			    content: '保存成功！',
        			    state: 'success'
        			 });
                    $.bhPropertyDialog.hide({
                    	close: function() {
                    		safe.reloadDatatable();
                    	}
                    });//关闭当前弹窗
    			});
        	}
        },
        dialogClose: function() {
        	$.bhPropertyDialog.hide();
        },
        applyAdd: function(e) {
        	var id = $(e.target).attr("data-x-wid");
        	var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_MGR_QUERY', {WID:id, pageNumber:1});
        	
        	var applySaveTpl = utils.loadCompiledPage('applySave');
            $.bhPaperPileDialog.show({
              content: applySaveTpl.render({year: pageMeta.params.year}),
              ready: function($header, $body, $footer){
            	  applySave.initialize(data.rows[0]);
              }
            });
        },
        proFinish: function() {
        	var row = $('#tableStatusPass').emapdatatable("checkedRecords");
        	if (row.length > 0) {
        		$.each(row, function() {
        			this.STATUS = 10;
        		});
        		bs.savePro({
        			T_XMBM_SUMMER_PRO_SAVE: JSON.stringify(row)
        		}).done(function(data){
                	$.bhTip({
        			    content: '项目完成！',
        			    state: 'success'
        			});
                	$('#tableStatusPass').emapdatatable('reload');
                });
        	}
        },
        reloadDatatable: function() {
        	$('#tableStatusWait').emapdatatable('reload');
    		$('#tableStatusPass').emapdatatable('reload');
    		$('#tableStatusFinish').emapdatatable('reload');
    		$('#tableStatusFail').emapdatatable('reload');
        }
    };
    return viewConfig;
});