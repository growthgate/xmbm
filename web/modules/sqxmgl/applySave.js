define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./sqxmglBS');
    var proData = null;
    var config = {};

    var viewConfig = {
        initialize: function(data) {
        	var safe = this;
        	proData = data;
        	this._initConfig();
        	
        	// 学年学期
        	$('[name="TERM"]').val(config.current_term);
        	// 学生基本信息
        	$('[name="STU_NUMBER"]').on('input', function(e) {
        		var id = $(this).val();
        		utils.doAjax("../modules/sqxmgl/hqyhxxxx.do", {id: id}).done(function(data) {
            		var info = data.datas.hqyhxxxx;
            		if (info.ID) {
            			$('[name="NAME"]').val(info.NAME);
            			$('[name="GENDER"]').val(info.GENDER);
            			$('[name="SCHOOL"]').val(info.SCHOOL);
            			$('[name="MAJOR"]').val(info.MAJOR);
            		} else {
            			$('[name="NAME"]').val('');
            			$('[name="GENDER"]').val('');
            			$('[name="SCHOOL"]').val('');
            			$('[name="MAJOR"]').val('');
            		}
            	});
        	});
        	// 报名项目
        	this._initProject();
        	// 英语水平
        	this._initSelect(contextPath + '/code/9dd65d60-7442-489e-9ba7-d3849292feec/eng_level.do', 'ENGLISH_LEVEL');
        	// 项目结束后我校互认学分
        	this._initSelect(contextPath + '/code/9dd65d60-7442-489e-9ba7-d3849292feec/course_nature.do', 'COURSE_NATURE');
        	// 个人陈述
        	$('#psInfo').text(config.personal_statement);
        	// 上传附件
        	if (!config.attachment_type) {
        		config.attachment_type = "";
        	}
        	$('#attachment').emapFileUpload({
        		contextPath: contextPath,
        		multiple: true,
        		type: config.attachment_type.split(","),
        		limit: config.attachment_limit,
        		size: config.attachment_size
        	});
        	// 附件描述
        	$('[name="PROJECT"]').on('change', function() {
        		attachment = $(this).find('option:selected').data('attachment');
        		safe.changeAttachmentInfo(attachment);
        	});
        	// 表单验证
        	$("#emapForm").validationEngine({
        		maxErrorsPerField: 1
        	});
        	
            this.eventMap = {
                '[data-action=submit]': this.submit
            };
        },
        submit: function(){
        	if (!$("#emapForm").validationEngine("validate")) {
        		BH_UTILS.bhDialogWarning({
        			content: '请检查输入的内容！',
        			buttons: [{text: '确定'}]
        		});
        	} else {
        		// 获取表单内容
    			var formData = $("#emapForm").serializeJson();
        		// 上传文件
        		$('#attachment').emapFileUpload('saveUpload').done(function(resolve) {
        			formData.ATTACHMENT = resolve.token;
        			bs.savePro(formData).done(function(data){
        				$.bhTip({
        					content: '数据保存成功！',
        					state: 'success'
        				});
        				$.bhPaperPileDialog.hide({
        					//关闭当前弹窗
        					close: function() {
        						$('#tableStatusWait').emapdatatable('reload');
        					}
        				});
        			});
        		});
        	}
        },
        _initConfig: function() {
    		var data = utils.doSyncAjax('../modules/sqxmgl/hqxtpz.do', {pageNumber: 1, pageSize: 100});
    		var rows = data.datas.hqxtpz.rows;
        	$.each(rows, function(idx, row) {
        		config[row.NAME] = row.VALUE;
        	});
        },
        _initSelect: function(dictUrl, name) {
        	var $dom = $('[name="' + name + '"]');
        	utils.doAjax(dictUrl).done(function(data) {
        		var rows = data.datas.code.rows;
        		$.each(rows, function(i, row) {
        			$dom.append('<option value="' + row.id + '">' + row.name + '</option>');
        		});
        	});
        },
        _initProject: function() {
        	utils.doAjax('../modules/sqxmgl/T_XMBM_SUMMER_PRO_MGR_QUERY.do').done(function(data) {
        		var $project = $('[name="PROJECT"]');
        		var rows = data.datas.T_XMBM_SUMMER_PRO_MGR_QUERY.rows;
        		$.each(rows, function(i, row) {
        			$project.append('<option value="' + row.WID + '" data-attachment="' + row.ATTACHMENT + '">' + row.NAME + '</option>');
        		});
        		$project.val(proData.WID);
        		$project.trigger('change');
        	});
        },
        changeAttachmentInfo: function(attachment) {
        	if (!attachment) {
        		attachment = "";
        	}
			$('#attachmentInfo').text(attachment);
        }
        
    };
    return viewConfig;
});