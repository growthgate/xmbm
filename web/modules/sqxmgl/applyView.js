define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./sqxmglBS');

    var viewConfig = {
        initialize: function(data) {
        	var mode = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_QUERY', 'form');
            $("#applyForm").emapForm({
                root:WIS_EMAP_SERV.getContextPath(),
                data: mode,
                model: 'h',
                cols: 4,
                readonly:true
            });
            $("#applyForm").emapForm("setValue", data);
            
            switch(data.STATUS) {
            case '-3':
            case '3':
            case '10':
            	$('#auditDetail').text('审核详情');
            	$('#auditTable').emapdatatable({
            		pagePath: bs.api.pageModel,
                    action: 'T_XMBM_SUMMER_PRO_AUDIT_QUERY',
                    params: {
                    	PRO_ID: data.WID
                    },
                    pageable: false,
                    minLineNum: 0
            	});
            	break;
            case '-10':
            	$('#auditDetail').html('中止详情');
            	$('#auditTable').emapdatatable({
            		pagePath: bs.api.pageModel,
                    action: 'T_XMBM_STOPPED_PRO_QUERY',
                    params: {
                    	PRO_ID: data.WID
                    },
                    pageable: false,
                    minLineNum: 0
            	});
            	break;
            default:
            }
            
            this.eventMap = {
            };
        }
    };
    return viewConfig;
});