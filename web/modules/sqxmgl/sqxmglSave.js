define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./sqxmglBS');

    var viewConfig = {
        initialize: function() {
        	var mode = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_MGR_QUERY', 'form');
            $("#emapForm").emapForm({
                root:WIS_EMAP_SERV.getContextPath(),//附件上传时必备属性
                data: mode,
                model: 'h',
                inputWidth: 8
            });
            $('[data-name="DESCRIPTION"]').emapEditor({
            	contextPath: WIS_EMAP_SERV.getContextPath()
            });
            
            this.eventMap = {
                '[data-action=save]': this.save
            };
        },
        save: function(){
        	if( $("#emapForm").emapValidate('validate') ){
        		var formData = $("#emapForm").emapForm("getValue");
        		formData.DESCRIPTION = $('[data-name="DESCRIPTION"]').emapEditor('getValue');
        		$("#emapForm").emapForm("saveUpload");//上传附件时使用
        		bs.save(formData).done(function(data){
        			$.bhTip({
        			    content: '数据保存成功！',
        			    state: 'success'
        			 });
                    $.bhPaperPileDialog.hide({
                    	close: function() {
                    		$('#emapdatatable').emapdatatable('reload');
                    	}
                    });//关闭当前弹窗
    			});
        	}
        }

    };
    return viewConfig;
});