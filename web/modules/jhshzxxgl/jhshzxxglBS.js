﻿define(function(require, exports, module) {
	var utils = require('utils');
	var bs = {
		api: {
			pageModel: 'modules/jhshzxxgl.do',
			resultProfile: './mock/resultProfile.json'
		},
		getDemoMainModel: function() {
			var def = $.Deferred();
			utils.doAjax(bs.api.resultProfile, null, 'get').done(function(data) {
				data.length = data.list.length;
				def.resolve(data);
			}).fail(function(res) {
				def.reject(res);
			});
			return def.promise();
		},
		save: function(formData){
			return BH_UTILS.doAjax('../modules/jhshzxxgl/T_XMBM_EXCHANGE_SCHOOL_SAVE.do', formData);
		},
		del: function(params){
			return BH_UTILS.doAjax('../modules/jhshzxxgl/T_XMBM_EXCHANGE_SCHOOL_DELETE.do', {
				T_XMBM_EXCHANGE_SCHOOL_DELETE:JSON.stringify(params)
			});
		},
		saveLimit: function(formData) {
			return BH_UTILS.doAjax('../modules/jhshzxxgl/T_XMBM_EXCHANGE_LIMIT_SAVE.do', {
				T_XMBM_EXCHANGE_LIMIT_SAVE: JSON.stringify(formData)
			});
		},
		delLimit: function(params) {
			return BH_UTILS.doAjax('../modules/jhshzxxgl/T_XMBM_EXCHANGE_LIMIT_DELETE.do', {
				T_XMBM_EXCHANGE_LIMIT_DELETE: JSON.stringify(params)
			});
		},
		savePro: function(formData) {
			return BH_UTILS.doAjax('../modules/jhshzxxgl/T_XMBM_EXCHANGE_PRO_SAVE.do', formData);
		},
		audit: function(data) {
			return BH_UTILS.doAjax('../modules/jhshzxxgl/jhssh.do', data);
		},
		stop: function(data) {
			return BH_UTILS.doAjax('../modules/jhshzxxgl/xmzz.do', data);
		},
		exportData: function(obj){
			var params = {
					root: contextPath,
					app : "xmbm",
					module : "modules",
					page : 'jhshzxxgl',
					action : 'T_XMBM_EXCHANGE_SCHOOL_QUERY'
			};
			//选择字段导出
			$('#emapdatatable').emapdatatable('selectColumnsExport', params);	
		}
	};

	return bs;
});