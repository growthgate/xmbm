define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./jhshzxxglBS');
    var proData = null;
    var config = {};

    var viewConfig = {
        initialize: function(data) {
        	var self = this;
        	proData = data;
        	this._initConfig();
        	
        	// 学年学期
        	$('[name="TERM"]').val(config.current_term);
        	// 学生基本信息
        	$('[name="STU_NUMBER"]').on('input', function(e) {
        		var id = $(this).val();
        		utils.doAjax("../modules/jhshzxxgl/hqyhxxxx.do", {id: id}).done(function(data) {
            		var info = data.datas.hqyhxxxx;
            		if (info.ID) {
            			$('[name="NAME"]').val(info.NAME);
            			$('[name="GENDER"]').val(info.GENDER);
            			$('[name="SCHOOL"]').val(info.SCHOOL);
            			$('[name="MAJOR"]').val(info.MAJOR);
            			$('[name="ID_NUMBER"]').val(info.ID_NUMBER);
            		} else {
            			$('[name="NAME"]').val('');
            			$('[name="GENDER"]').val('');
            			$('[name="SCHOOL"]').val('');
            			$('[name="MAJOR"]').val('');
            			$('[name="ID_NUMBER"]').val('');
            		}
            	});
        	});
        	// 身份证有效期
        	$('#idValidTerm').jqxDateTimeInput({
        		culture: 'zh-CN',
        		formatString: 'yyyy-MM-dd'
        	});
        	$('#inputidValidTerm').prop('name', 'ID_VALID_TERM');
        	// 报名项目
        	this._initSelect(contextPath + '/code/9dd65d60-7442-489e-9ba7-d3849292feec/exchange_pro.do', 'PROJECT');
        	// 首选学校
        	$('[name="PROJECT"]').on('change', function() {
        		self.changeChoiceSchool($(this).val());
        	});
        	// 上传附件
        	var attachment_type;
        	if (!config.attachment_type) {
        		attachment_type = [];
        	} else {
        		attachment_type = config.attachment_type.split(",");
        	}
        	$('#attachment').emapFileUpload({
        		contextPath: contextPath,
        		multiple: true,
        		type: attachment_type,
        		limit: config.attachment_limit,
        		size: config.attachment_size
        	});
        	// 附件描述
        	$('#attachmentInfo').text(config.exchange_pro_attachment);
        	// 表单验证
        	$("#emapForm").validationEngine({
        		maxErrorsPerField: 1
        	});
        	
            this.eventMap = {
                '[data-action=submit]': this.submit
            };
        },
        submit: function(){
        	if (!$("#emapForm").validationEngine("validate")) {
        		BH_UTILS.bhDialogWarning({
        			content: '请检查输入的内容！',
        			buttons: [{text: '确定'}]
        		});
        	} else {
        		// 获取表单内容
    			var formData = $("#emapForm").serializeJson();
        		// 上传文件
        		$('#attachment').emapFileUpload('saveUpload').done(function(resolve) {
        			formData.ATTACHMENT = resolve.token;
        			bs.savePro(formData).done(function(data){
        				$.bhTip({
        					content: '数据保存成功！',
        					state: 'success'
        				});
        				$.bhPaperPileDialog.hide({
        					//关闭当前弹窗
        					close: function() {
        						$('#tableStatusWait').emapdatatable('reload');
        					}
        				});
        			});
        		});
        	}
        },
        _initConfig: function() {
    		var data = utils.doSyncAjax('../modules/jhshzxxgl/hqxtpz.do', {pageNumber: 1, pageSize: 100});
    		var rows = data.datas.hqxtpz.rows;
        	$.each(rows, function(idx, row) {
        		config[row.NAME] = row.VALUE;
        	});
        },
        _initSelect: function(dictUrl, name) {
        	var $dom = $('[name="' + name + '"]');
        	utils.doAjax(dictUrl).done(function(data) {
        		var rows = data.datas.code.rows;
        		$.each(rows, function(i, row) {
        			$dom.append('<option value="' + row.id + '">' + row.name + '</option>');
        		});
        		$dom.val(proData.PRO_TYPE);
        		$dom.trigger('change');
        	});
        },
        changeChoiceSchool: function(type) {
        	var $dom = $('[name="CHOICE_SCHOOL"]');
        	$dom.empty().append('<option value="">请选择...</option>');
        	if (type) {
        		utils.doAjax('../modules/jhshzxxgl/T_XMBM_EXCHANGE_SCHOOL_QUERY.do', {PRO_TYPE: type}).done(function(data) {
            		var rows = data.datas.T_XMBM_EXCHANGE_SCHOOL_QUERY.rows;
            		$.each(rows, function(i, row) {
            			$dom.append('<option value="' + row.WID + '">' + row.NAME + '</option>');
            		});
            		$dom.val(proData.WID);
            		$dom.trigger('change');
            	});
        	}
        }

    };
    return viewConfig;
});