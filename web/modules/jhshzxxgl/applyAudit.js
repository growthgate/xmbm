define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./jhshzxxglBS');

    var viewConfig = {
        initialize: function(data) {
        	var mode = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_EXCHANGE_PRO_QUERY', 'form');
            $("#applyForm").emapForm({
                root:WIS_EMAP_SERV.getContextPath(),
                data: mode,
                model: 'h',
                cols: 4,
                readonly:true
            });
            $("#applyForm").emapForm("setValue", data);
            
            $('#auditDetail').text('审核详情');
        	$('#auditTable').emapdatatable({
        		pagePath: bs.api.pageModel,
                action: 'jhsxyshxx',
                params: {
                	PRO_ID: data.WID
                },
                pageable: false,
                minLineNum: 0
        	});
            
            var auditMode = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_EXCHANGE_AUDIT_QUERY', 'form');
        	$("#auditForm").emapForm({
        		root:WIS_EMAP_SERV.getContextPath(),//附件上传时必备属性
        		data: auditMode,
        		model: 'h'
        	});
        	// 将学生选择的学校和经费渠道作为默认值
        	$("#auditForm").emapForm('setValue', {
        		SCHOOL: data.CHOICE_SCHOOL,
        		EXPENSE: data.CAN_ONE_OWN_EXPENSE
        	});
        	// 推荐老师默认为当前老师
        	utils.doAjax("../modules/jhshzxxgl/hqyhxxxx.do", {}).done(function(data) {
        		var info = data.datas.hqyhxxxx;
        		$("#auditForm").emapForm('setValue', {
        			TEACHER: info.NAME
            	});
        	});
        	$('[data-action="auditPass"]').show();
        	$('[data-action="auditFail"]').show();
        	
            this.eventMap = {
            	'[data-action=auditPass]': this.pass,
            	'[data-action=auditFail]': this.fail
            };
        },
        pass: function() {
        	this.save(3);
        },
        fail: function() {
        	this.save(-3);
        },
        save: function(status) {
        	if ($("#auditForm").emapValidate('validate')) {
        		var applyData = $("#applyForm").emapForm("getValue");
        		var auditData = $("#auditForm").emapForm("getValue");
        		applyData.STATUS = status;
        		auditData.PRO_ID = applyData.WID;
        		//$("#emapForm").emapForm("saveUpload");//上传附件时使用
        		bs.audit({
        			savePro: JSON.stringify(applyData),
        			saveAudit: JSON.stringify(auditData)
        		}).done(function(data){
        			$.bhTip({
        			    content: '审核成功！',
        			    state: 'success'
        			 });
                    $.bhPaperPileDialog.hide({
                    	close: function() {
                    		$('#tableStatusWait').emapdatatable('reload');
                    	}
                    });//关闭当前弹窗
    			});
        	}
        }
    };
    return viewConfig;
});