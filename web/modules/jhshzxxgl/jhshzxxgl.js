﻿define(function(require, exports, module) {

  var utils = require('utils');
  var bs = require('./jhshzxxglBS');
  var jhshzxxglSave = require('./jhshzxxglSave');
  var jhshzxxglView = require('./jhshzxxglView');
  var apply = require('./apply');
  var applyAudit = require('./applyAudit');
  var applySave = require('./applySave');

  var viewConfig = {
    initialize: function() {
      var view = utils.loadCompiledPage('jhshzxxgl');
      this.$rootElement.html(view.render({}), true);
      this.pushSubView([jhshzxxglSave, jhshzxxglView, apply, applyAudit, applySave]);
      this.initView();

      this.eventMap = {
        "[data-action=add]": this.actionAdd,
        "[data-action=edit]": this.actionEdit,
        "[data-action=detail]": this.actionDetail,
        "[data-action=delete]": this.actionDelete,
        "[data-action=apply]": this.actionApply
      };
    },

    initView: function() {
      this._initAdvanceQuery();
      this._initTable();
    },

    actionAdd: function(){
      var jhshzxxglNewTpl = utils.loadCompiledPage('jhshzxxglSave');
      $.bhPaperPileDialog.show({
        content: jhshzxxglNewTpl.render({}),
        title: "新建",
        ready: function($header, $body, $footer){
          jhshzxxglSave.initialize();
        }
      });
    },
        
    actionEdit: function(e){
      var id = $(e.target).attr("data-x-wid");
      var jhshzxxglEditTpl = utils.loadCompiledPage('jhshzxxglSave');
      var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_EXCHANGE_SCHOOL_QUERY', {WID:id, pageNumber:1});
          
      $.bhPaperPileDialog.show({
        content: jhshzxxglEditTpl.render({}),
        title: "编辑",
        ready: function($header, $body, $footer){
          jhshzxxglSave.initialize();       
          $("#emapForm").emapForm("setValue", data.rows[0]);
          $('[data-name="DESCRIPTION"]').emapEditor('setValue', data.rows[0].DESCRIPTION);
        }
      });
    },
        
    actionDetail: function(e){
      var id = $(e.target).attr("data-x-wid");
      var jhshzxxglViewTpl = utils.loadCompiledPage('jhshzxxglView');
      var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_EXCHANGE_SCHOOL_QUERY', {WID:id, pageNumber:1});
          
      $.bhPaperPileDialog.show({
        content: jhshzxxglViewTpl.render({}),
        title: "查看",
        ready: function($header, $body, $footer){
          jhshzxxglView.initialize(data.rows[0]);
        }
      });
    },
        
    actionDelete: function(){
      var row = $("#emapdatatable").emapdatatable("checkedRecords");
      if(row.length > 0){
        var params = row.map(function(el){
        	return {WID:el.WID};  //模型主键
        });
        bs.del(params).done(function(data){
        	$.bhTip({
			    content: '数据删除成功！',
			    state: 'success'
			});
        	$('#emapdatatable').emapdatatable('reload');
        });
      }
    },
    
    actionApply: function(e){
    	var id = $(e.target).attr("data-x-wid");
        var applyTpl = utils.loadCompiledPage('apply');
            
        $.bhPaperPileDialog.show({
          content: applyTpl.render({proId: id}),
          ready: function($header, $body, $footer){
            apply.initialize(id);
          }
        });
    },
        
    _initAdvanceQuery: function() {
      var searchData = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_EXCHANGE_SCHOOL_QUERY', "search");
      var $query = $('#emapAdvancedQuery').emapAdvancedQuery({
        data: searchData,
        contextPath : contextPath,
        schema: true
      });
      $query.on('search', this._searchCallback);
    },

    _searchCallback: function(e, data, opts) {
      $('#emapdatatable').emapdatatable('reloadFirstPage', {
        querySetting: data
      });
    },

    _initTable: function() {
      var tableOptions = {
        pagePath: bs.api.pageModel,
        action: 'T_XMBM_EXCHANGE_SCHOOL_QUERY',
        customColumns: [{
          colIndex: '0',
          type: 'checkbox'
        }, {
          colIndex: '1',
          type: 'tpl',
          width: '200px',
          column: {
            text: '操作',
            align: 'center',
            cellsAlign: 'center',
            cellsRenderer: function(row, column, value, rowData) {
              return '<a href="javascript:void(0)" data-action="detail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>'
                + ' | <a href="javascript:void(0)" data-action="edit" data-x-wid=' + rowData.WID + '>' + '编辑' + '</a>'
                + ' | <a href="javascript:void(0)" data-action="apply" data-x-wid=' + rowData.WID + '>' + '查看已报名学生' + '</a>';
            }
          }
        }]
      };
      $('#emapdatatable').emapdatatable(tableOptions);
    }
  };

  return viewConfig;
});