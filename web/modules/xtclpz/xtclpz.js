﻿define(function(require, exports, module) {

  var utils = require('utils');
  var bs = require('./xtclpzBS');
  var xtclpzSave = require('./xtclpzSave');
  var xtclpzView = require('./xtclpzView');

  var viewConfig = {
    initialize: function() {
      var view = utils.loadCompiledPage('xtclpz');
      this.$rootElement.html(view.render({}), true);
      this.pushSubView([xtclpzSave]);
      this.initView();

      this.eventMap = {
    	"[data-action=add]": this.actionAdd,
        "[data-action=edit]": this.actionEdit,
        "[data-action=detail]": this.actionDetail
      };
    },

    initView: function() {
      this._initAdvanceQuery();
      this._initTable();
    },
    
    actionAdd: function(){
      var xtclpzNewTpl = utils.loadCompiledPage('xtclpzSave');
      $.bhPaperPileDialog.show({
        content: xtclpzNewTpl.render({}),
        title: "新建",
        ready: function($header, $body, $footer){
        	xtclpzSave.initialize();
        }
      });
    },

    actionEdit: function(e){
      var id = $(e.target).attr("data-x-wid");
      var xtclpzEditTpl = utils.loadCompiledPage('xtclpzSave');
      var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_SYS_CONFIG_QUERY', {WID:id, pageNumber:1});
          
      $.bhPaperPileDialog.show({
        content: xtclpzEditTpl.render({}),
        title: "编辑",
        ready: function($header, $body, $footer){
          xtclpzSave.initialize();       
          $("#emapForm").emapForm("setValue", data.rows[0]);    
        }
      });
    },
        
    actionDetail: function(e){
      var id = $(e.target).attr("data-x-wid");
      var xtclpzViewTpl = utils.loadCompiledPage('xtclpzSave');
      var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_SYS_CONFIG_QUERY', {WID:id, pageNumber:1});
          
      $.bhPaperPileDialog.show({
        content: xtclpzViewTpl.render({}),
        title: "查看",
        ready: function($header, $body, $footer){
          xtclpzView.initialize(data.rows[0]);
        }
      });
    },
        
    _initAdvanceQuery: function() {
      var searchData = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_SYS_CONFIG_QUERY', "search");
      var $query = $('#emapAdvancedQuery').emapAdvancedQuery({
        data: searchData,
        contextPath : contextPath,
        schema: true
      });
      $query.on('search', this._searchCallback);
    },

    _searchCallback: function(e, data, opts) {
      $('#emapdatatable').emapdatatable('reloadFirstPage', {
        querySetting: data
      });
    },

    _initTable: function() {
      var tableOptions = {
        pagePath: bs.api.pageModel,
        action: 'T_XMBM_SYS_CONFIG_QUERY',
        customColumns: [{
          colIndex: '0',
          type: 'tpl',
          column: {
            text: '操作',
            align: 'center',
            cellsAlign: 'center',
            cellsRenderer: function(row, column, value, rowData) {
              return '<a href="javascript:void(0)" data-action="detail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>'
                + ' | <a href="javascript:void(0)" data-action="edit" data-x-wid=' + rowData.WID + '>' + '编辑' + '</a>';
            }
          }
        }]
      };
      $('#emapdatatable').emapdatatable(tableOptions);
    }
  };

  return viewConfig;
});