define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./sqxmbmBS');

    var viewConfig = {
        initialize: function(data) {
        	var mode = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_QUERY', 'form');
            $("#emapForm").emapForm({
                root:WIS_EMAP_SERV.getContextPath(),
                data: mode,
                model: 'h',
                cols: 4,
                readonly:true
            });
            $("#emapForm").emapForm("setValue", data);
            
            // 如果已审核，显示审核详情
            var status = parseInt(data.STATUS);
            switch (status) {
            case 3:
            case 10:
            	$('#auditDetail').html('审核详情');
            	$('#auditTable').emapdatatable({
            		pagePath: bs.api.pageModel,
                    action: 'T_XMBM_SUMMER_PRO_AUDIT_QUERY',
                    params: {
                    	PRO_ID: data.WID
                    },
                    pageable: false,
                    minLineNum: 0
            	});
            	break;
            case -10:
            	$('#auditDetail').html('中止详情');
            	$('#auditTable').emapdatatable({
            		pagePath: bs.api.pageModel,
                    action: 'T_XMBM_STOPPED_PRO_QUERY',
                    params: {
                    	PRO_ID: data.WID
                    },
                    pageable: false,
                    minLineNum: 0
            	});
            default:
            }
            
            $("[data-action=save]").hide();
            this.eventMap = {
            };
        }
    };
    return viewConfig;
});