﻿define(function(require, exports, module) {

  var utils = require('utils');
  var bs = require('./sqxmbmBS');
  var sqxmbmSave = require('./sqxmbmSave');
  var sqxmbmView = require('./sqxmbmView');
  var proView = require('./proView');

  var viewConfig = {
    initialize: function() {
      var view = utils.loadCompiledPage('sqxmbm');
      this.$rootElement.html(view.render({}), true);
      this.pushSubView([sqxmbmSave]);
      this.initView();

      this.eventMap = {
        "[data-action=add]": this.actionAdd,
        "[data-action=edit]": this.actionEdit,
        "[data-action=detail]": this.actionDetail,
        "[data-action=pro-detail]": this.actionProDetail,
        "[data-action=cancel]": this.actionCancel
      };
    },

    initView: function() {
      this._initAdvanceQuery();
      this._initTable();
    },

    actionAdd: function(){
      var sqxmbmNewTpl = utils.loadCompiledPage('sqxmbmSave');
      $("#title").removeClass("bh-hide");
      $.bhPaperPileDialog.show({
        content: sqxmbmNewTpl.render({year: pageMeta.params.year}),
        ready: function($header, $body, $footer){
          sqxmbmSave.initialize();
        },
        close: function() {
        	$("#title").addClass("bh-hide");
        }
      });
    },
        
    actionEdit: function(e){
      var id = $(e.target).attr("data-x-wid");
      var sqxmbmEditTpl = utils.loadCompiledPage('sqxmbmSave');
      var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_QUERY', {WID:id, pageNumber:1});
      $("#title").removeClass("bh-hide");
      $.bhPaperPileDialog.show({
        content: sqxmbmEditTpl.render({}),
        ready: function($header, $body, $footer){
          sqxmbmSave.initialize(data.rows[0]);       
        },
        close: function() {
        	$("#title").addClass("bh-hide");
        }
      });
    },
        
    actionDetail: function(e){
      var id = $(e.target).attr("data-x-wid");
      var sqxmbmViewTpl = utils.loadCompiledPage('sqxmbmView');
      var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_QUERY', {WID:id, pageNumber:1});
          
      $("#title").removeClass("bh-hide");
      $.bhPaperPileDialog.show({
        content: sqxmbmViewTpl.render({}),
        title: "查看",
        ready: function($header, $body, $footer){
          sqxmbmView.initialize(data.rows[0]);
        },
        close: function() {
        	$("#title").addClass("bh-hide");
        }
      });
    },
    
    actionProDetail: function(e){
        var id = $(e.target).attr("data-x-wid");
        var sqxmbmViewTpl = utils.loadCompiledPage('sqxmbmView');
        var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_MGR_QUERY', {WID:id, pageNumber:1});
            
        $("#title").removeClass("bh-hide");
        $.bhPaperPileDialog.show({
          content: sqxmbmViewTpl.render({}),
          title: "查看",
          ready: function($header, $body, $footer){
        	  proView.initialize(data.rows[0]);
          },
          close: function() {
          	$("#title").addClass("bh-hide");
          }
        });
    },
        
    actionCancel: function(e){
    	var id = $(e.target).attr("data-x-wid");
		bs.save({WID: id, STATUS: -1}).done(function(data){
        	$.bhTip({
			    content: '撤回成功！',
			    state: 'success'
			});
        	$('#emapdatatable').emapdatatable('reload');
        });
    },
        
    _initAdvanceQuery: function() {
      var searchData = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_QUERY', "search");
      var $query = $('#emapAdvancedQuery').emapAdvancedQuery({
        data: searchData,
        contextPath : contextPath,
        schema: true
      });
      $query.on('search', this._searchCallback);
    },

    _searchCallback: function(e, data, opts) {
      $('#emapdatatable').emapdatatable('reloadFirstPage', {
        querySetting: data
      });
    },

    _initTable: function() {
      var tableOptions = {
        pagePath: bs.api.pageModel,
        action: 'T_XMBM_SUMMER_PRO_QUERY',
        customColumns: [{
          colIndex: '0',
          type: 'tpl',
          column: {
            text: '操作',
            align: 'center',
            cellsAlign: 'center',
            width: '140px',
            cellsRenderer: function(row, column, value, rowData) {
            	var str = "";
            	str += '<a href="javascript:void(0)" data-action="detail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>';
            	if (rowData.STATUS === "1") {
            		str += ' | <a href="javascript:void(0)" data-action="edit" data-x-wid=' + rowData.WID + '>' + '编辑' + '</a>';
            		str += ' | <a href="javascript:void(0)" data-action="cancel" data-x-wid=' + rowData.WID + '>' + '撤回申请' + '</a>';
            	}
                return str;
            }
          }
        }]
      };
      $('#emapdatatable').emapdatatable(tableOptions);
      
      var projectTableObtions = {
        pagePath: bs.api.pageModel,
        action: 'hqsqxmlb1',
        customColumns: [{
            colIndex: '0',
            type: 'tpl',
            column: {
              text: '操作',
              align: 'center',
              cellsAlign: 'center',
              cellsRenderer: function(row, column, value, rowData) {
            	return '<a href="javascript:void(0)" data-action="pro-detail" data-x-wid=' + rowData.PRO_ID + '>' + '查看项目详情' + '</a>';
              }
            }
          }]
      };
      $('#projectDatatable').emapdatatable(projectTableObtions);
    }
  };

  return viewConfig;
});