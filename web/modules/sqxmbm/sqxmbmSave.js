define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./sqxmbmBS');
    var config = {};
    var rowData = null;

    var viewConfig = {
        initialize: function(data) {
        	var safe = this;
        	this._initConfig();
        	rowData = data;
        	
        	// 学年学期
        	$('[name="TERM"]').val(config.current_term);
        	// 学生基本信息
        	utils.doAjax("../modules/sqxmbm/hqyhxxxx.do").done(function(data) {
        		var info = data.datas.hqyhxxxx;
        		if (info.ID) {
        			$('[name="STU_NUMBER"]').val(info.ID).prop('readonly', true);
        			$('[name="NAME"]').val(info.NAME).prop('readonly', true);
        			$('[name="GENDER"]').val(info.GENDER).prop('readonly', true);
        			$('[name="SCHOOL"]').val(info.SCHOOL).prop('readonly', true);
        			$('[name="MAJOR"]').val(info.MAJOR).prop('readonly', true);
        		} else {
        			$('[name="STU_NUMBER"]').val(pageMeta.params.userid).prop('readonly', true);
        		}
        	});
        	// 报名项目
        	this._initProject();
        	// 英语水平
        	this._initSelect(contextPath + '/code/9dd65d60-7442-489e-9ba7-d3849292feec/eng_level.do', 'ENGLISH_LEVEL');
        	// 项目结束后我校互认学分
        	this._initSelect(contextPath + '/code/9dd65d60-7442-489e-9ba7-d3849292feec/course_nature.do', 'COURSE_NATURE');
        	// 个人陈述
        	$('#psInfo').text(config.personal_statement);
        	// 上传附件
        	if (!config.attachment_type) {
        		config.attachment_type = "";
        	}
        	var token = null;
        	if (rowData) {
        		token = rowData.ATTACHMENT;
        	}
        	$('#attachment').emapFileUpload({
        		contextPath: contextPath,
        		multiple: true,
        		token: token,
        		type: config.attachment_type.split(","),
        		limit: config.attachment_limit,
        		size: config.attachment_size
        	});
        	// 附件描述
        	$('[name="PROJECT"]').on('change', function() {
        		attachment = $(this).find('option:selected').data('attachment');
        		safe.changeAttachmentInfo(attachment);
        	});
        	// 同意信息
        	var agreeInfo = config.summer_pro_agreement;
        	if (agreeInfo) {
        		$('#agreeInfo').text(agreeInfo);
        		$('#agree').on('change', function() {
        			var isChecked = $(this).is(':checked');
        			$('[data-action="submit"]').prop('disabled', !isChecked);
        		});
        	} else {
        		$('#agreeDiv').hide();
        	}
        	// 表单验证
        	$("#emapForm").validationEngine({
        		maxErrorsPerField: 1
        	});
        	
        	// 如果为编辑
        	if (rowData) {
        		$('[name="WID"]').val(rowData.WID);
        		$('[name="EMAIL"]').val(rowData.EMAIL);
        		$('[name="TEL"]').val(rowData.TEL);
        		$('[name="PASSPORT_NUMBER"]').val(rowData.PASSPORT_NUMBER);
        		$('[name="ENGLISH_SCORE"]').val(rowData.ENGLISH_SCORE);
        		$('[name="CREDIT"]').val(rowData.CREDIT);
        		$('[name="PS"]').val(rowData.PS);
        	}
        	
            this.eventMap = {
                '[data-action=submit]': this.submit
            };
        },
        submit: function(){
        	if (!$("#emapForm").validationEngine("validate")) {
        		BH_UTILS.bhDialogWarning({
        			content: '请检查输入的内容！',
        			buttons: [{text: '确定'}]
        		});
        	} else {
        		// 获取表单内容
    			var formData = $("#emapForm").serializeJson();
    			if (!rowData) {
    				// 判断是否有报过名
                	var data = utils.doSyncAjax('../modules/sqxmbm/jcsqxmzfbm.do', {
                		STU_NUMBER: formData.STU_NUMBER,
                		TERM: formData.TERM,
                		PROJECT: formData.PROJECT
                	});
                	if (data.datas.jcsqxmzfbm) {
                		BH_UTILS.bhDialogWarning({
                			content: '一个项目只能报一次名！',
                			buttons: [{text: '确定'}]
                		});
        				return;
                	}
    			}
        		// 上传文件
        		$('#attachment').emapFileUpload('saveUpload').done(function(resolve) {
        			formData.ATTACHMENT = resolve.token;
        			bs.save(formData).done(function(data){
        				$.bhTip({
        					content: '数据保存成功！',
        					state: 'success'
        				});
        				$.bhPaperPileDialog.hide({
        					//关闭当前弹窗
        					close: function() {
        						$('#emapdatatable').emapdatatable('reload');
                        		$('#projectDatatable').emapdatatable('reload');
        					}
        				});
        			});
        		});
        	}
        },
        _initConfig: function() {
    		var data = utils.doSyncAjax('../modules/sqxmbm/hqxtpz.do', {pageNumber: 1, pageSize: 100});
    		var rows = data.datas.hqxtpz.rows;
        	$.each(rows, function(idx, row) {
        		config[row.NAME] = row.VALUE;
        	});
        },
        _initSelect: function(dictUrl, name) {
        	var $dom = $('[name="' + name + '"]');
        	utils.doAjax(dictUrl).done(function(data) {
        		var rows = data.datas.code.rows;
        		$.each(rows, function(i, row) {
        			$dom.append('<option value="' + row.id + '">' + row.name + '</option>');
        		});
        		if (rowData) {
        			$dom.val(rowData[name]);
        			$dom.trigger('change');
        		}
        	});
        },
        _initProject: function() {
        	utils.doAjax('../modules/sqxmbm/hqksqsqxmlb1.do').done(function(data) {
        		var $project = $('[name="PROJECT"]');
        		var rows = data.datas.hqksqsqxmlb1.rows;
        		$.each(rows, function(i, row) {
        			$project.append('<option value="' + row.WID + '" data-attachment="' + row.ATTACHMENT + '">' + row.NAME + '</option>');
        		});
        		if (rowData) {
        			$project.val(rowData.PROJECT);
        			$project.trigger('change');
        		}
        	});
        },
        changeAttachmentInfo: function(attachment) {
        	if (!attachment) {
        		attachment = "";
        	}
			$('#attachmentInfo').text(attachment);
        }
        
    };
    return viewConfig;
});