define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./sqxmbmBS');

    var viewConfig = {
        initialize: function(data) {
        	var mode = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_MGR_QUERY', 'form');
            $("#emapForm").emapForm({
                root:WIS_EMAP_SERV.getContextPath(),
                data: mode,
                model: 'h',
                readonly:true
            });
            $("#emapForm").emapForm("setValue", data);
            $('[data-name="DESCRIPTION"]').replaceWith('<div class="note-editor"><div class="note-editable panel-body">' + data.DESCRIPTION + '</div></div>');
            
            $("[data-action=save]").hide();
            this.eventMap = {
            };
        }
    };
    return viewConfig;
});