﻿define(function(require, exports, module) {
	var utils = require('utils');
	var bs = {
		api: {
			pageModel: 'modules/xyjhssh.do',
			resultProfile: './mock/resultProfile.json'
		},
		getDemoMainModel: function() {
			var def = $.Deferred();
			utils.doAjax(bs.api.resultProfile, null, 'get').done(function(data) {
				data.length = data.list.length;
				def.resolve(data);
			}).fail(function(res) {
				def.reject(res);
			});
			return def.promise();
		},
		audit: function(data) {
			return BH_UTILS.doAjax('../modules/xyjhssh/jhssh.do', data);
		},
		exportData: function(obj){
			var params = {
					root: contextPath,
					app : "xmbm",
					module : "modules",
					page : 'xyjhssh',
					action : 'T_XMBM_EXCHANGE_SCHOOL_QUERY'
			};
			//选择字段导出
			$('#emapdatatable').emapdatatable('selectColumnsExport', params);	
		}
	};

	return bs;
});