﻿define(function(require, exports, module) {

  var utils = require('utils');
  var bs = require('./xsnjglBS');
  var xsnjglSave = require('./xsnjglSave');
  var xsnjglView = require('./xsnjglView');

  var viewConfig = {
    initialize: function() {
      var view = utils.loadCompiledPage('xsnjgl');
      this.$rootElement.html(view.render({}), true);
      this.pushSubView([xsnjglSave]);
      this.initView();

      this.eventMap = {
        "[data-action=add]": this.actionAdd,
        "[data-action=edit]": this.actionEdit,
        "[data-action=detail]": this.actionDetail,
        "[data-action=delete]": this.actionDelete
      };
    },

    initView: function() {
      this._initAdvanceQuery();
      this._initTable();
    },

    actionAdd: function(){
      var xsnjglNewTpl = utils.loadCompiledPage('xsnjglSave');
      $.bhPaperPileDialog.show({
        content: xsnjglNewTpl.render({}),
        title: "新建",
        ready: function($header, $body, $footer){
          xsnjglSave.initialize();
        }
      });
    },
        
    actionEdit: function(e){
      var id = $(e.target).attr("data-x-wid");
      var xsnjglEditTpl = utils.loadCompiledPage('xsnjglSave');
      var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_GRADE_QUERY', {WID:id, pageNumber:1}).rows[0];
          
      $.bhPaperPileDialog.show({
        content: xsnjglEditTpl.render({}),
        title: "编辑",
        ready: function($header, $body, $footer){
          xsnjglSave.initialize();
          // 参数转换：将[1/0]转换为[是/否]bug简单解决方式 也可以选择建立新的业务模型来解决
          if (data.IS_DROPDOWN == '是') {
        	  data.IS_DROPDOWN = 1;
          } else {
        	  data.IS_DROPDOWN = 0;
          }
          $("#emapForm").emapForm("setValue", data);
        }
      });
    },
        
    actionDetail: function(e){
      var id = $(e.target).attr("data-x-wid");
      var xsnjglViewTpl = utils.loadCompiledPage('xsnjglSave');
      var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_GRADE_QUERY', {WID:id, pageNumber:1});
          
      $.bhPaperPileDialog.show({
        content: xsnjglViewTpl.render({}),
        title: "查看",
        ready: function($header, $body, $footer){
          xsnjglView.initialize(data.rows[0]);
        }
      });
    },
        
    actionDelete: function(){
      var row = $("#emapdatatable").emapdatatable("checkedRecords");
      if(row.length > 0){
	    var params = row.map(function(el){
          return {WID:el.WID};  //模型主键
        });
        bs.del(params).done(function(data){
        	$.bhTip({
			    content: '数据删除成功！',
			    state: 'success'
			});
        	$('#emapdatatable').emapdatatable('reload');
        });
      }
    },
        
    _initAdvanceQuery: function() {
      var searchData = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_GRADE_QUERY', "search");
      var $query = $('#emapAdvancedQuery').emapAdvancedQuery({
        data: searchData,
        contextPath : contextPath,
        schema: true
      });
      $query.on('search', this._searchCallback);
    },

    _searchCallback: function(e, data, opts) {
      $('#emapdatatable').emapdatatable('reloadFirstPage', {
        querySetting: data
      });
    },

    _initTable: function() {
      var tableOptions = {
        pagePath: bs.api.pageModel,
        action: 'T_XMBM_GRADE_QUERY',
        customColumns: [{
          colIndex: '0',
          type: 'checkbox'
        }, {
          colIndex: '1',
          type: 'tpl',
          column: {
            text: '操作',
            align: 'center',
            cellsAlign: 'center',
            cellsRenderer: function(row, column, value, rowData) {
              return '<a href="javascript:void(0)" data-action="detail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>'
                + ' | <a href="javascript:void(0)" data-action="edit" data-x-wid=' + rowData.WID + '>' + '编辑' + '</a>';
            }
          }
        }]
      };
      $('#emapdatatable').emapdatatable(tableOptions);
    }
  };

  return viewConfig;
});