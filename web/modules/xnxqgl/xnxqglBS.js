﻿define(function(require, exports, module) {
	var utils = require('utils');
	var bs = {
		api: {
			pageModel: 'modules/xnxqgl.do',
			resultProfile: './mock/resultProfile.json'
		},
		getDemoMainModel: function() {
			var def = $.Deferred();
			utils.doAjax(bs.api.resultProfile, null, 'get').done(function(data) {
				data.length = data.list.length;
				def.resolve(data);
			}).fail(function(res) {
				def.reject(res);
			});
			return def.promise();
		},
		save: function(formData){
			return BH_UTILS.doAjax('../modules/xnxqgl/T_XMBM_TERM_SAVE.do', formData);
		},
		del: function(params){
			return BH_UTILS.doAjax('../modules/xnxqgl/T_XMBM_TERM_DELETE.do', {
				T_XMBM_TERM_DELETE:JSON.stringify(params)
			});
		},
		exportData: function(obj){
			var params = {
					root: contextPath,
					app : "xmbm",
					module : "modules",
					page : 'xnxqgl',
					action : 'T_XMBM_TERM_QUERY'
			};
			//选择字段导出
			$('#emapdatatable').emapdatatable('selectColumnsExport', params);	
		}
	};

	return bs;
});