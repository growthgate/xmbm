define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./xysqxmshBS');
    var applyView = require('./applyView');
    var applyAudit = require('./applyAudit');

    var viewConfig = {
        initialize: function(id) {
        	var safe = this;
        	// 获取当前老师所在学院
        	utils.doAjax("../modules/xysqxmsh/hqyhxxxx.do", {}).done(function(data) {
        		var info = data.datas.hqyhxxxx;
        		/*
            	 * 待审核
            	 */
            	$('#tableStatus1').emapdatatable({
    				pagePath: bs.api.pageModel,
    				action: 'cxsqxmbmlb',
    				params: {
    					PROJECT: id,
    					STATUS: '1',
    					SCHOOL: info.SCHOOL
    				},
    				customColumns: [{
    					colIndex: '0',
    					type: 'tpl',
    					column: {
    						text: '操作',
    						align: 'center',
    						cellsAlign: 'center',
    						cellsRenderer: function(row, column, value, rowData) {
    							return '<a href="javascript:void(0)" data-action="applyDetail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>'
    									+ ' | <a href="javascript:void(0)" data-action="audit" data-x-wid=' + rowData.WID + '>' + '审核' + '</a>';
    						}
    					}
    				}]
            	});
            	/*
            	 * 审核通过
            	 */
            	$('#tableStatusPass').emapdatatable({
    				pagePath: bs.api.pageModel,
    				action: 'cxsqxmbmlb',
    				params: {
    					PROJECT: id,
    					STATUS: '3,10',
    					SCHOOL: info.SCHOOL
    				},
    				customColumns: [{
    					colIndex: '0',
    					type: 'tpl',
    					column: {
    						text: '操作',
    						align: 'center',
    						cellsAlign: 'center',
    						cellsRenderer: function(row, column, value, rowData) {
    							return '<a href="javascript:void(0)" data-action="applyDetail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>';
    						}
    					}
    				}]
            	});
            	/*
            	 * 项目未完成
            	 */
            	$('#tableStatusFail').emapdatatable({
            		pagePath: bs.api.pageModel,
    				action: 'cxsqxmbmlb',
    				params: {
    					PROJECT: id,
    					STATUS: '-1,-3,-10',
    					SCHOOL: info.SCHOOL
    				},
    				customColumns: [{
    					colIndex: '0',
    					type: 'tpl',
    					column: {
    						text: '操作',
    						align: 'center',
    						cellsAlign: 'center',
    						cellsRenderer: function(row, column, value, rowData) {
    							return '<a href="javascript:void(0)" data-action="applyDetail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>';
    						}
    					}
    				}]
            	});
        	});
        	$('#applyTab').jqxTabs();
        	$('#applyTab').on('tabclick', function() {
        		safe.reloadDatatable();
        	});
        	
        	this.eventMap = {
        		"[data-action=applyDetail]": this.applyDetail,
        		"[data-action=audit]": this.audit
            };
        },
        applyDetail: function(e) {
        	var id = $(e.target).attr("data-x-wid");
            var applyViewTpl = utils.loadCompiledPage('applyView');
            var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_QUERY', {WID:id});
                
            $.bhPaperPileDialog.show({
              content: applyViewTpl.render({}),
              title: "查看",
              ready: function($header, $body, $footer){
            	  applyView.initialize(data.rows[0]);
              }
            });
        },
        audit: function(e) {
        	var id = $(e.target).attr("data-x-wid");
            var applyAuditTpl = utils.loadCompiledPage('applyView');
            var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_QUERY', {WID:id, pageNumber:1});
                
            $.bhPaperPileDialog.show({
              content: applyAuditTpl.render({}),
              title: "审核",
              ready: function($header, $body, $footer){
            	  applyAudit.initialize(data.rows[0]);
              }
            });
        },
        reloadDatatable: function() {
        	$('#tableStatus1').emapdatatable('reload');
    		$('#tableStatusPass').emapdatatable('reload');
    		$('#tableStatusFail').emapdatatable('reload');
        }
    };
    return viewConfig;
});