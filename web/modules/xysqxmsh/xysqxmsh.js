﻿define(function(require, exports, module) {

  var utils = require('utils');
  var bs = require('./xysqxmshBS');
  var xysqxmshView = require('./xysqxmshView');
  var apply = require('./apply');
  var applyAudit = require('./applyAudit');

  var viewConfig = {
    initialize: function() {
      var view = utils.loadCompiledPage('xysqxmsh');
      this.$rootElement.html(view.render({}), true);
      this.pushSubView([apply, applyAudit]);
      this.initView();

      this.eventMap = {
        "[data-action=detail]": this.actionDetail,
        "[data-action=apply]": this.actionApply
      };
    },

    initView: function() {
      this._initAdvanceQuery();
      this._initTable();
    },
        
    actionDetail: function(e){
      var id = $(e.target).attr("data-x-wid");
      var xysqxmshViewTpl = utils.loadCompiledPage('xysqxmshView');
      var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_MGR_QUERY', {WID:id});
          
      $.bhPaperPileDialog.show({
        content: xysqxmshViewTpl.render({}),
        title: "查看",
        ready: function($header, $body, $footer){
          xysqxmshView.initialize(data.rows[0]);
        }
      });
    },
    
    actionApply: function(e) {
    	var id = $(e.target).attr("data-x-wid");
        var applyTpl = utils.loadCompiledPage('apply');
            
        $.bhPaperPileDialog.show({
          content: applyTpl.render({}),
          ready: function($header, $body, $footer){
            apply.initialize(id);
          }
        });
    },
        
    _initAdvanceQuery: function() {
      var searchData = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_SUMMER_PRO_MGR_QUERY', "search");
      var $query = $('#emapAdvancedQuery').emapAdvancedQuery({
        data: searchData,
        contextPath : contextPath,
        schema: true
      });
      $query.on('search', this._searchCallback);
    },

    _searchCallback: function(e, data, opts) {
      $('#emapdatatable').emapdatatable('reloadFirstPage', {
        querySetting: data
      });
    },

    _initTable: function() {
      var tableOptions = {
        pagePath: bs.api.pageModel,
        action: 'T_XMBM_SUMMER_PRO_MGR_QUERY',
        customColumns: [{
          colIndex: '0',
          type: 'tpl',
          width: '160px',
          column: {
            text: '操作',
            align: 'center',
            cellsAlign: 'center',
            cellsRenderer: function(row, column, value, rowData) {
              return '<a href="javascript:void(0)" data-action="detail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>'
                + ' | <a href="javascript:void(0)" data-action="apply" data-x-wid=' + rowData.WID + '>' + '查看已报名学生' + '</a>';
            }
          }
        }]
      };
      $('#emapdatatable').emapdatatable(tableOptions);
    }
  };

  return viewConfig;
});