define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./jhsbmBS');
    var config = {};
    var rowData = null;

    var viewConfig = {
        initialize: function(data) {
        	this._initConfig();
        	rowData = data;
        	
        	// 学年学期
        	$('[name="TERM"]').val(config.current_term);
        	// 学生基本信息
        	utils.doAjax("../modules/jhsbm/hqyhxxxx.do", {}).done(function(data) {
        		var info = data.datas.hqyhxxxx;
        		if (info.ID) {
        			$('[name="STU_NUMBER"]').val(info.ID).prop('readonly', true);
        			$('[name="NAME"]').val(info.NAME).prop('readonly', true);
        			$('[name="GENDER"]').val(info.GENDER).prop('readonly', true);
        			$('[name="SCHOOL"]').val(info.SCHOOL).prop('readonly', true);
        			$('[name="MAJOR"]').val(info.MAJOR).prop('readonly', true);
        			$('[name="ID_NUMBER"]').val(info.ID_NUMBER).prop('readonly', true);
        		} else {
        			$('[name="STU_NUMBER"]').val(pageMeta.params.userid).prop('readonly', true);
        		}
        	});
        	// 身份证有效期
        	$('#idValidTerm').jqxDateTimeInput({
        		culture: 'zh-CN',
        		formatString: 'yyyy-MM-dd'
        	});
        	$('#inputidValidTerm').prop('name', 'ID_VALID_TERM');
        	// 报名项目
        	this._initSelect(contextPath + '/code/9dd65d60-7442-489e-9ba7-d3849292feec/exchange_pro.do', 'PROJECT');
        	// 首选学校
        	$('[name="PROJECT"]').on('change', function() {
        		viewConfig.changeChoiceSchool($(this).val());
        	});
        	// 上传附件
        	var attachment_type;
        	if (!config.attachment_type) {
        		attachment_type = [];
        	} else {
        		attachment_type = config.attachment_type.split(",");
        	}
        	var token = null;
        	if (rowData) {
        		token = rowData.ATTACHMENT;
        	}
        	$('#attachment').emapFileUpload({
        		contextPath: contextPath,
        		multiple: true,
        		token: token,
        		type: attachment_type,
        		limit: config.attachment_limit,
        		size: config.attachment_size
        	});
        	// 附件描述
        	$('#attachmentInfo').text(config.exchange_pro_attachment);
        	// 同意信息
        	var agreeInfo = config.exchange_pro_agreement;
        	if (agreeInfo) {
        		$('#agreeInfo').text(agreeInfo);
        		$('#agree').on('change', function() {
        			var isChecked = $(this).is(':checked');
        			$('[data-action="submit"]').prop('disabled', !isChecked);
        		});
        	} else {
        		$('#agreeDiv').hide();
        	}
        	// 表单验证
        	$("#emapForm").validationEngine({
        		maxErrorsPerField: 1
        	});
        	
        	// 如果为编辑
        	if (rowData) {
        		$('[name="WID"]').val(rowData.WID);
        		$('[name="TEL"]').val(rowData.TEL);
        		$('#idValidTerm').jqxDateTimeInput('setDate', rowData.ID_VALID_TERM);
        		$('[name="PASSPORT_NUMBER"]').val(rowData.PASSPORT_NUMBER);
        		$('[name="APPLY_SCHOOL"]').val(rowData.APPLY_SCHOOL);
        		$('[name="APPLY_MAJOR"]').val(rowData.APPLY_MAJOR);
        		$('[name="CAN_ADJUST"]').prop('checked', rowData.CAN_ADJUST);
        		$('[name="CAN_ONE_OWN_EXPENSE"]').prop('checked', rowData.CAN_ONE_OWN_EXPENSE);
        		$('[name="PS"]').val(rowData.PS);
        	}
        	
            this.eventMap = {
                '[data-action=submit]': this.submit
            };
        },
        submit: function(){
        	if (!$("#emapForm").validationEngine("validate")) {
        		BH_UTILS.bhDialogWarning({
        			content: '请检查输入的内容！',
        			buttons: [{text: '确定'}]
        		});
        	} else {
        		// 获取表单内容
    			var formData = $("#emapForm").serializeJson();
    			if (!rowData) {
    				// 判断是否有报过名
                	var data = utils.doSyncAjax('../modules/jhsbm/jcjhszfbm.do', {
                		STU_NUMBER: formData.STU_NUMBER,
                		TERM: formData.TERM,
                		CHOICE_SCHOOL: formData.CHOICE_SCHOOL
                	});
                	if (data.datas.jcjhszfbm) {
                		BH_UTILS.bhDialogWarning({
                			content: '一个项目只能报一次名！',
                			buttons: [{text: '确定'}]
                		});
        				return;
                	}
    			}
        		// 上传文件
        		$('#attachment').emapFileUpload('saveUpload').done(function(resolve) {
        			formData.ATTACHMENT = resolve.token;
        			bs.save(formData).done(function(data){
        				$.bhTip({
        					content: '数据保存成功！',
        					state: 'success'
        				});
        				$.bhPaperPileDialog.hide({
        					//关闭当前弹窗
        					close: function() {
        						$('#emapdatatable').emapdatatable('reload');
        					}
        				});
        			});
        		});
        	}
        },
        _initConfig: function() {
    		var data = utils.doSyncAjax('../modules/jhsbm/hqxtpz.do', {pageNumber: 1, pageSize: 100});
    		var rows = data.datas.hqxtpz.rows;
        	$.each(rows, function(idx, row) {
        		config[row.NAME] = row.VALUE;
        	});
        },
        _initSelect: function(dictUrl, name) {
        	var $dom = $('[name="' + name + '"]');
        	utils.doAjax(dictUrl).done(function(data) {
        		var rows = data.datas.code.rows;
        		$.each(rows, function(i, row) {
        			$dom.append('<option value="' + row.id + '">' + row.name + '</option>');
        		});
        		if (rowData) {
        			$dom.val(rowData[name]);
        			$dom.trigger('change');
        		}
        	});
        },
        changeChoiceSchool: function(type) {
        	var $dom = $('[name="CHOICE_SCHOOL"]');
        	$dom.empty().append('<option value="">请选择...</option>');
        	if (type) {
        		utils.doAjax('../modules/jhsbm/hqksqjhsxmlb.do', {proType: type}).done(function(data) {
            		var rows = data.datas.hqksqjhsxmlb.rows;
            		$.each(rows, function(i, row) {
            			$dom.append('<option value="' + row.WID + '">' + row.NAME + '</option>');
            		});
            		if (rowData) {
            			$dom.val(rowData.CHOICE_SCHOOL);
            			$dom.trigger('change');
            		}
            	});
        	}
        }

    };
    return viewConfig;
});