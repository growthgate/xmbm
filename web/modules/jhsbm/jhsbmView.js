define(function(require, exports, module) {
    var utils = require('utils');
    var bs = require('./jhsbmBS');

    var viewConfig = {
        initialize: function(data) {
        	var mode = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_EXCHANGE_PRO_QUERY', 'form');
            $("#emapForm").emapForm({
                root:WIS_EMAP_SERV.getContextPath(),
                data: mode,
                model: 'h',
                cols: 4,
                readonly:true
            });
            $("#emapForm").emapForm("setValue", data);
            
            var status = parseInt(data.STATUS);
            switch (status) {
            case 2:
            	$('#auditDetail').html('审核详情');
            	$('#auditTable').emapdatatable({
            		pagePath: bs.api.pageModel,
                    action: 'jhsxyshxx',
                    params: {
                    	PRO_ID: data.WID
                    },
                    pageable: false,
                    minLineNum: 0
            	});
            	break;
            case 3:
            case 10:
            	$('#auditDetail').html('审核详情');
            	$('#auditTable').emapdatatable({
            		pagePath: bs.api.pageModel,
                    action: 'T_XMBM_EXCHANGE_AUDIT_QUERY',
                    params: {
                    	PRO_ID: data.WID
                    },
                    pageable: false,
                    minLineNum: 0
            	});
            	break;
            case -10:
            	$('#auditDetail').html('中止详情');
            	$('#auditTable').emapdatatable({
            		pagePath: bs.api.pageModel,
                    action: 'T_XMBM_STOPPED_PRO_QUERY',
                    params: {
                    	PRO_ID: data.WID
                    },
                    pageable: false,
                    minLineNum: 0
            	});
            default:
            }
            
            $("[data-action=save]").hide();
            this.eventMap = {
            };
        }
    };
    return viewConfig;
});