﻿define(function(require, exports, module) {

  var utils = require('utils');
  var bs = require('./jhsbmBS');
  var jhsbmSave = require('./jhsbmSave');
  var jhsbmView = require('./jhsbmView');
  var proView = require('./proView');
  var config = {};

  var viewConfig = {
    initialize: function() {
      var view = utils.loadCompiledPage('jhsbm');
      this.$rootElement.html(view.render({}), true);
      this.pushSubView([jhsbmSave]);
      this.initView();

      this.eventMap = {
        "[data-action=add]": this.actionAdd,
        "[data-action=edit]": this.actionEdit,
        "[data-action=detail]": this.actionDetail,
        "[data-action=pro-detail]": this.actionProDetail,
        "[data-action=cancel]": this.actionCancel
      };
    },

    initView: function() {
      this._initAdvanceQuery();
      this._initTable();
      this._initConfig();
    },

    actionAdd: function(){
      var jhsbmNewTpl = utils.loadCompiledPage('jhsbmSave');
      $("#title").removeClass("bh-hide");
      $.bhPaperPileDialog.show({
        content: jhsbmNewTpl.render({year: config.current_quarter}),
        ready: function($header, $body, $footer){
          jhsbmSave.initialize();
        },
		close: function() {
			$("#title").addClass("bh-hide");
		}
      });
    },
    
    actionEdit: function(e){
        var id = $(e.target).attr("data-x-wid");
        var jhsbmEditTpl = utils.loadCompiledPage('jhsbmSave');
        var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_EXCHANGE_PRO_QUERY', {WID:id, pageNumber:1});
        $("#title").removeClass("bh-hide");
        $.bhPaperPileDialog.show({
          content: jhsbmEditTpl.render({year: config.current_quarter}),
          ready: function($header, $body, $footer){
        	  jhsbmSave.initialize(data.rows[0]);
          },
          close: function() {
          	$("#title").addClass("bh-hide");
          }
        });
    },
        
    actionDetail: function(e){
      var id = $(e.target).attr("data-x-wid");
      var jhsbmViewTpl = utils.loadCompiledPage('jhsbmView');
      var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_EXCHANGE_PRO_QUERY', {WID:id, pageNumber:1});
          
      $("#title").removeClass("bh-hide");
      $.bhPaperPileDialog.show({
		content: jhsbmViewTpl.render({}),
		title: "查看",
		ready: function($header, $body, $footer){
		  jhsbmView.initialize(data.rows[0]);
		},
		close: function() {
			$("#title").addClass("bh-hide");
		}
      });
    },
    
    actionProDetail: function(e){
        var id = $(e.target).attr("data-x-wid");
        var jhsbmViewTpl = utils.loadCompiledPage('jhsbmView');
        var data = WIS_EMAP_SERV.getData(bs.api.pageModel, 'T_XMBM_EXCHANGE_SCHOOL_QUERY', {WID:id, pageNumber:1});
            
        $("#title").removeClass("bh-hide");
        $.bhPaperPileDialog.show({
          content: jhsbmViewTpl.render({}),
          title: "查看",
          ready: function($header, $body, $footer){
        	  proView.initialize(data.rows[0]);
          },
          close: function() {
          	$("#title").addClass("bh-hide");
          }
        });
    },
    
    actionCancel: function(e){
    	var id = $(e.target).attr("data-x-wid");
		bs.save({WID: id, STATUS: -1}).done(function(data){
        	$.bhTip({
			    content: '撤回成功！',
			    state: 'success'
			});
        	$('#emapdatatable').emapdatatable('reload');
        });
    },
        
    _initAdvanceQuery: function() {
      var searchData = WIS_EMAP_SERV.getModel(bs.api.pageModel, 'T_XMBM_EXCHANGE_PRO_QUERY', "search");
      var $query = $('#emapAdvancedQuery').emapAdvancedQuery({
        data: searchData,
        contextPath : contextPath,
        schema: true
      });
      $query.on('search', this._searchCallback);
    },

    _searchCallback: function(e, data, opts) {
      $('#emapdatatable').emapdatatable('reloadFirstPage', {
        querySetting: data
      });
    },

    _initTable: function() {
      var tableOptions = {
        pagePath: bs.api.pageModel,
        action: 'T_XMBM_EXCHANGE_PRO_QUERY',
        customColumns: [{
          colIndex: '0',
          type: 'tpl',
          column: {
            text: '操作',
            align: 'center',
            cellsAlign: 'center',
            width: '140px',
            cellsRenderer: function(row, column, value, rowData) {
            	var str = "";
            	str += '<a href="javascript:void(0)" data-action="detail" data-x-wid=' + rowData.WID + '>' + '详情' + '</a>';
            	if (rowData.STATUS === "1") {
            		str += ' | <a href="javascript:void(0)" data-action="edit" data-x-wid=' + rowData.WID + '>' + '编辑' + '</a>';
            		str += ' | <a href="javascript:void(0)" data-action="cancel" data-x-wid=' + rowData.WID + '>' + '撤回申请' + '</a>';
            	}
                return str;
            }
          }
        }]
      };
      $('#emapdatatable').emapdatatable(tableOptions);
      
      var projectTableObtions = {
        pagePath: bs.api.pageModel,
        action: 'hqjhsxmlb1',
        customColumns: [{
            colIndex: '0',
            type: 'tpl',
            column: {
              text: '学校名称',
              align: 'center',
              cellsAlign: 'center',
              width: '25%',
              cellsRenderer: function(row, column, value, rowData) {
            	var array = new Array();
            	var ids = rowData.PRO_ID.split(",");
            	var names = rowData.PRO_ID_DISPLAY.split(",");
            	$.each(ids, function(idx, id) {
            		var a = '<a href="javascript:void(0)" data-action="pro-detail" data-x-wid=' + id + '>' + names[idx] + '</a>';
            		array.push(a);
            	});
            	return array.join(' ');
              }
            }
          }]
      };
      $('#projectDatatable').emapdatatable(projectTableObtions);
    },
    
    _initConfig: function() {
		var data = utils.doSyncAjax('../modules/sqxmbm/hqxtpz.do', {pageNumber: 1, pageSize: 100});
		var rows = data.datas.hqxtpz.rows;
    	$.each(rows, function(idx, row) {
    		config[row.NAME] = row.VALUE;
    	});
    }
  };

  return viewConfig;
});